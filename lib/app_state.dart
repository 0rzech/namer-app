import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class MyAppState extends ChangeNotifier {
  var _current = WordPair.random();
  var _favorites = <WordPair>{};
  var _history = <WordPair>[];

  GlobalKey? historyListKey;

  WordPair get current => _current;
  Set<WordPair> get favorites => _favorites;
  List<WordPair> get history => _history;

  void getNext() {
    history.insert(0, _current);
    final list = historyListKey?.currentState as AnimatedListState?;
    list?.insertItem(0);
    _current = WordPair.random();
    notifyListeners();
  }

  void toggleFavorite([WordPair? pair]) {
    pair = pair ?? _current;
    if (_favorites.contains(pair)) {
      _favorites.remove(pair);
    } else {
      _favorites.add(pair);
    }
    notifyListeners();
  }

  void removeFavorite(final WordPair pair) {
    _favorites.remove(pair);
    notifyListeners();
  }
}
