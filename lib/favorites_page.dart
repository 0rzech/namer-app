import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'word_pair_extension.dart';
import 'app_state.dart';

class FavoritesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final theme = Theme.of(context);

    if (appState.favorites.isEmpty) {
      final bold = theme.textTheme.bodyLarge!.copyWith(fontWeight: FontWeight.bold);
      return Center(
        child: Text(
          'No favorites yet.',
          style: bold,
        ),
      );
    }

    final bold = theme.textTheme.bodyMedium!.copyWith(fontWeight: FontWeight.bold);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Text(
            'You have ${appState.favorites.length} favorites:',
            style: bold,
          ),
        ),
        Expanded(
          child: GridView(
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 350,
              childAspectRatio: 5 / 1,
            ),
            children: [
              for (final pair in appState.favorites)
                ListTile(
                  leading: IconButton(
                    icon: const Icon(Icons.delete_outlined),
                    color: theme.colorScheme.primary,
                    onPressed: () => appState.removeFavorite(pair),
                  ),
                  title: Text(
                    pair.asLowerCase,
                    style: bold,
                    semanticsLabel: pair.asLowerCaseSpaceSplit,
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }
}
