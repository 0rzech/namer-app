import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:english_words/english_words.dart';
import 'word_pair_extension.dart';
import 'app_state.dart';

class GeneratorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var pair = appState.current;

    final icon = appState.favorites.contains(pair) ? Icons.favorite : Icons.favorite_border;
    final theme = Theme.of(context);
    final style = theme.textTheme.labelLarge!.copyWith(
      fontWeight: FontWeight.bold,
      color: theme.colorScheme.primary,
    );

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: 3,
            child: _HistoryView(),
          ),
          SizedBox(height: 10),
          _BigCard(pair: pair),
          SizedBox(height: 10),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton.icon(
                onPressed: appState.toggleFavorite,
                icon: Icon(icon),
                label: Text(
                  'Like',
                  style: style,
                ),
              ),
              SizedBox(width: 10),
              ElevatedButton(
                onPressed: appState.getNext,
                child: Text(
                  'Next',
                  style: style,
                ),
              ),
            ],
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }
}

class _BigCard extends StatelessWidget {
  const _BigCard({required this.pair});

  final WordPair pair;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final style = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.onPrimary,
    );
    final light = style.copyWith(fontWeight: FontWeight.w200);
    final bold = style.copyWith(fontWeight: FontWeight.bold);

    return Card(
      color: theme.colorScheme.primary,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: AnimatedSize(
          duration: const Duration(milliseconds: 200),
          child: Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: pair.first,
                  style: light,
                ),
                TextSpan(
                  text: pair.second,
                  style: bold,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _HistoryView extends StatefulWidget {
  @override
  State<_HistoryView> createState() => _HistoryViewState();
}

class _HistoryViewState extends State<_HistoryView> {
  final _key = GlobalKey();

  static const Gradient _maskingGradient = LinearGradient(
    colors: [Colors.transparent, Colors.black],
    stops: [0.0, 0.5],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );

  @override
  Widget build(BuildContext context) {
    final state = context.watch<MyAppState>();
    state.historyListKey = _key;

    return ShaderMask(
      shaderCallback: (bounds) => _maskingGradient.createShader(bounds),
      blendMode: BlendMode.dstIn,
      child: AnimatedList(
        key: _key,
        reverse: true,
        padding: EdgeInsets.only(top: 100),
        initialItemCount: state.history.length,
        itemBuilder: (context, index, animation) {
          final pair = state.history[index];
          final icon = state.favorites.contains(pair) ? Icon(Icons.favorite, size: 12) : SizedBox();
          final theme = Theme.of(context);
          final bold = theme.textTheme.labelMedium?.copyWith(
            fontWeight: FontWeight.bold,
            color: theme.colorScheme.primary,
          );

          return SizeTransition(
            sizeFactor: animation,
            child: Center(
              child: TextButton.icon(
                icon: icon,
                label: Text(
                  pair.asLowerCase,
                  style: bold,
                  semanticsLabel: pair.asLowerCaseSpaceSplit,
                ),
                onPressed: () => state.toggleFavorite(pair),
              ),
            ),
          );
        },
      ),
    );
  }
}
