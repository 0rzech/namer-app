import 'package:flutter/material.dart';
import 'generator_page.dart';
import 'favorites_page.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final page = switch (_selectedIndex) {
      0 => GeneratorPage(),
      1 => FavoritesPage(),
      _ => throw UnimplementedError('no widget at index $_selectedIndex'),
    };

    final theme = Theme.of(context);

    final pageArea = ColoredBox(
      color: theme.colorScheme.primaryContainer,
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 200),
        child: page,
      ),
    );

    const homeIcon = Icon(Icons.home);
    const homeLabel = 'Home';
    const favsIcon = Icon(Icons.favorite);
    const favsLabel = 'Favorites';

    return Scaffold(
      body: LayoutBuilder(builder: (context, constraints) {
        if (constraints.maxWidth < 450) {
          const labelTheme = TextStyle(fontWeight: FontWeight.bold);

          return Column(children: [
            Expanded(child: pageArea),
            BottomNavigationBar(
              items: [
                BottomNavigationBarItem(icon: homeIcon, label: homeLabel),
                BottomNavigationBarItem(icon: favsIcon, label: favsLabel),
              ],
              selectedLabelStyle: labelTheme,
              unselectedLabelStyle: labelTheme,
              currentIndex: _selectedIndex,
              onTap: _setSelectedIndex,
            ),
          ]);
        } else {
          final bold = theme.textTheme.labelLarge!.copyWith(fontWeight: FontWeight.bold);

          return Row(children: [
            SafeArea(
              child: NavigationRail(
                extended: constraints.maxWidth >= 700,
                destinations: [
                  NavigationRailDestination(
                    icon: homeIcon,
                    label: Text(homeLabel, style: bold),
                  ),
                  NavigationRailDestination(
                    icon: favsIcon,
                    label: Text(favsLabel, style: bold),
                  ),
                ],
                selectedIndex: _selectedIndex,
                onDestinationSelected: _setSelectedIndex,
              ),
            ),
            Expanded(child: pageArea),
          ]);
        }
      }),
    );
  }

  void _setSelectedIndex(int selectedIntex) {
    setState(() {
      _selectedIndex = selectedIntex;
    });
  }
}
