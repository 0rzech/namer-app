import 'package:english_words/english_words.dart';

extension WordPairExtension on WordPair {
  String get asLowerCaseSpaceSplit => '$first $second';
}
